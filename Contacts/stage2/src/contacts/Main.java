// https://stepik.org/submissions/602013/126888861?unit=178572 (with edits)
package contacts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    private static List<Contact> phonebook;
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        phonebook = new ArrayList<>();
        boolean flag = true;
        while (flag) {
            System.out.print("Enter action (add, remove, edit, count, list, exit):\n");
            String action = sc.nextLine();
            switch (action) {
                case "add":
                    modeAdd(sc);
                    break;
                case "remove":
                    modeRemove(sc);
                    break;
                case "edit":
                    modeEdit(sc);
                    break;
                case "count":
                    printCount();
                    break;
                case "list":
                    printList();
                    break;
                case "exit":
                    flag = false;
                    break;
                default:
                    break;
            }
        }
        sc.close();
    }
    
    private static void modeAdd(Scanner sc) {
        System.out.print("Enter the name:\n");
        String name = sc.nextLine();
        System.out.print("Enter surname:\n");
        String surname = sc.nextLine();
        System.out.print("Enter number:\n");
        String number = sc.nextLine();
        var contact = new Contact(name, surname, number);
        phonebook.add(contact);
        if (!contact.hasNumber()) {
            System.out.println("Wrong number format!");
        }
        System.out.println("The record added.");
    }
    
    private static void modeEdit(Scanner sc) {
        if (phonebook.size() == 0) {
            System.out.println("No records to edit!");
        } else {
            printList();
            System.out.print("Select a record:\n");
            int recordNumber = Integer.parseInt(sc.nextLine()) - 1;
            System.out.print("Select a field (name, surname, number):\n");
            String field = sc.nextLine();
            switch (field) {
                case "name":
                    System.out.print("Enter name:\n");
                    phonebook.get(recordNumber).setName(sc.nextLine());
                    break;
                case "surname":
                    System.out.print("Enter surname:\n");
                    phonebook.get(recordNumber).setSurname(sc.nextLine());
                    break;
                case "number":
                    System.out.print("Enter number:\n");
                    phonebook.get(recordNumber).setPhoneNumber(sc.nextLine());
                    break;
                default:
                    break;
            }
            System.out.println("The record updated!");
        }
    }
    
    private static void modeRemove(Scanner sc) {
        if (phonebook.size() == 0) {
            System.out.println("No records to remove!");
        } else {
            printList();
            System.out.print("Select a record:\n");
            int recordNumber = Integer.parseInt(sc.nextLine()) - 1;
            phonebook.remove(recordNumber);
            System.out.println("The record removed!");
        }
    }
    
    private static void printCount() {
        System.out.printf("The Phone Book has %d records.\n", phonebook.size());
    }
    
    private static void printList() {
        for (int i = 0; i < phonebook.size(); i++) {
            System.out.printf("%d. %s\n", i + 1, phonebook.get(i));
        }
    }
}


class Contact {
    private String name;
    private String surname;
    private String phoneNumber;
    
    public Contact(String name, String surname, String phoneNumber) {
        this.name = name;
        this.surname = surname;
        this.phoneNumber = numberIsValid(phoneNumber) ? phoneNumber : "";
    }
    
    boolean hasNumber() {
        return this.phoneNumber.length() > 0;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getSurname() {
        return surname;
    }
    
    public void setSurname(String surname) {
        this.surname = surname;
    }
    
    public String getPhoneNumber() {
        return phoneNumber;
    }
    
    public void setPhoneNumber(String phoneNumber) {
        if (numberIsValid(phoneNumber)) {
            this.phoneNumber = phoneNumber;
        } else {
            this.phoneNumber = "";
            System.out.println("Wrong number format!");
        }
    }
    
    public String toString() {
        return String.format("%s %s, %s", name, surname, hasNumber() ? phoneNumber : "[no number]");
    }
    
    private static boolean numberIsValid(String phoneNumber) {
        String withoutPlus = phoneNumber.startsWith("+") ? phoneNumber.substring(1) : phoneNumber;
        var groups = Arrays.asList(withoutPlus.split("[ -]"));
        if (groups.isEmpty()) {
            return false;
        }
        var areWrapped = groups.stream().map(gr -> gr.startsWith("(") && gr.endsWith(")")).collect(Collectors.toList());
        if (areWrapped.stream().filter(wr -> wr).count() > 1 // no more than one is wrapped
                || areWrapped.stream().skip(2).anyMatch(wr -> wr)) { // no one except the two first is wrapped
            return false;
        }
        
        var pattern = Pattern.compile("[\\w\\d]+");
        for (int i = 0; i < groups.size(); i++) {
            var gr = groups.get(i);
            // unwrap to correctly check the size!! () - is incorrect, but 12 is correct
            if (areWrapped.get(i)) {
                gr = groups.get(i).substring(1, gr.length() - 1);
            }
            if (i != 0 && gr.length() < 2) {
                return false;
            }
            if (!pattern.matcher(gr).matches()) {
                return false;
            }
        }
        return true;
    }
}