// https://stepik.org/submissions/602014/127212122?unit=178572
package contacts;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Contact> contactList = new ArrayList<>();
        boolean terminate = false;
        
        while (!terminate) {
            System.out.print("\nEnter action (add, remove, edit, count, info, exit): > ");
            String action = scanner.next();
            
            if (Actions.ADD.name().equals(action.toUpperCase())) {
                ContactFactory personFactory = new PersonFactory();
                ContactFactory organizationFactory = new OrganizationFactory();
                
                System.out.print("Enter the type (person, organization): > ");
                scanner.nextLine();
                String typeName = scanner.nextLine();
                
                if ("person".equals(typeName)) {
                    Person person = (Person) personFactory.makeContact();
                    
                    System.out.print("Enter the name: > ");
                    person.setName(scanner.nextLine());
                    
                    System.out.print("Enter the surname: > ");
                    person.setSurname(scanner.nextLine());
                    
                    System.out.print("Enter the birth date: > ");
                    person.setBirthDate(scanner.nextLine());
                    
                    System.out.print("Enter the gender (M, F): > ");
                    person.setGender(scanner.nextLine());
                    
                    System.out.print("Enter the number: > ");
                    person.setNumber(scanner.nextLine());
                    
                    person.setTimeCreated(LocalDateTime.now());
                    person.setTimeEdited(LocalDateTime.now());
                    contactList.add(person);
                } else if ("organization".equals(typeName)) {
                    Organization organization = (Organization) organizationFactory.makeContact();
                    
                    System.out.print("Enter the organization name: > ");
                    organization.setName(scanner.nextLine());
                    
                    System.out.print("Enter the address: > ");
                    organization.setAddress(scanner.nextLine());
                    
                    System.out.print("Enter the number: > ");
                    organization.setNumber(scanner.nextLine());
                    
                    organization.setTimeCreated(LocalDateTime.now());
                    organization.setTimeEdited(LocalDateTime.now());
                    contactList.add(organization);
                }
                
                System.out.println("The record added.");
            }
            
            if (Actions.REMOVE.name().equals(action.toUpperCase())) {
                if (contactList.size() == 0) {
                    System.out.println("No records to remove!");
                } else {
                    printContactList(contactList);
                    
                    System.out.print("Select a record: > ");
                    int i = scanner.nextInt();
                    contactList.remove(i - 1);
                    System.out.println("The record removed!");
                }
            }
            
            if (Actions.EDIT.name().equals(action.toUpperCase())) {
                if (contactList.size() == 0) {
                    System.out.println("No records to edit!");
                } else {
                    printContactList(contactList);
                    
                    System.out.print("Select a record: > ");
                    int i = scanner.nextInt();
                    Contact contact = contactList.get(i - 1);
                    
                    if (contact.isPerson()) {
                        Person person = (Person) contact;
                        System.out.print("Select a field (name, surname, birth, gender, number): > ");
                        String field = scanner.next();
                        
                        if ("name".equals(field)) {
                            System.out.print("Enter name: > ");
                            person.setName(scanner.next());
                            System.out.println("The record updated!");
                        }
                        
                        if ("surname".equals(field)) {
                            System.out.print("Enter surname: > ");
                            person.setSurname(scanner.next());
                            System.out.println("The record updated!");
                        }
                        
                        if ("birth".equals(field)) {
                            System.out.print("Enter birth date");
                            person.setBirthDate(scanner.next());
                            System.out.println("The record updated!");
                        }
                        
                        if ("gender".equals(field)) {
                            System.out.print("Enter gender: > ");
                            person.setGender(scanner.next());
                            System.out.println("The record updated!");
                        }
                        
                        if ("number".equals(field)) {
                            System.out.print("Enter number: > ");
                            scanner.nextLine();
                            String prevNumber = person.getNumber();
                            person.setNumber(scanner.nextLine());
                            if (!person.getNumber().equals(prevNumber) && person.hasNumber()) {
                                System.out.println("The record updated!");
                            }
                        }
                        
                        person.setTimeEdited(LocalDateTime.now());
                    } else {
                        Organization organization = (Organization) contact;
                        System.out.print("Select field (name, address, number): > ");
                        String field = scanner.next();
                        
                        if ("name".equals(field)) {
                            System.out.print("Enter name: > ");
                            scanner.nextLine();
                            organization.setName(scanner.nextLine());
                            System.out.println("The record updated!");
                        }
                        
                        if ("address".equals(field)) {
                            System.out.print("Enter address: > ");
                            scanner.nextLine();
                            organization.setAddress(scanner.nextLine());
                            System.out.println("The record updated!");
                        }
                        
                        if ("number".equals(field)) {
                            System.out.println("Enter number: > ");
                            scanner.nextLine();
                            String prevNumber = organization.getNumber();
                            organization.setNumber(scanner.nextLine());
                            if (!organization.getNumber().equals(prevNumber) && organization.hasNumber()) {
                                System.out.println("The record updated!");
                            }
                        }
                        
                        organization.setTimeEdited(LocalDateTime.now());
                    }
                }
            }
            
            if (Actions.COUNT.name().equals(action.toUpperCase())) {
                System.out.println("The phone book has " + contactList.size() + " records.");
            }
            
            if (Actions.INFO.name().equals(action.toUpperCase())) {
                printContactList(contactList);
                System.out.print("Enter index to show info: > ");
                int i = scanner.nextInt();
                Contact contact = contactList.get(i - 1);
                
                if (contact.isPerson()) {
                    Person person = (Person) contact;
                    System.out.println(person.toString());
                } else {
                    Organization organization = (Organization) contact;
                    System.out.println(organization.toString());
                }
            }
            
            if (Actions.EXIT.name().equals(action.toUpperCase())) {
                terminate = true;
            }
        }
    }
    
    private static void printContactList(List<Contact> contactList) {
        for (int i = 0; i < contactList.size(); i++) {
            Contact contact = contactList.get(i);
            if (contact.getClass() == Person.class) {
                Person person = (Person) contact;
                System.out.println((i + 1) + ". " + person.getName() + " " + person.getSurname());
            } else {
                Organization organization = (Organization) contact;
                System.out.println((i + 1) + ". " + organization.getName());
            }
        }
    }
    
    enum Actions {
        ADD, REMOVE, EDIT, COUNT, INFO, EXIT
    }
}

class Contact {
    
    private String number = "";
    private LocalDateTime timeCreated;
    private LocalDateTime timeEdited;
    private boolean isPerson = false;
    
    String getNumber() {
        return number;
    }
    
    void setNumber(String number) {
        if (!checkNumber(number) && !hasNumber()) {
            System.out.println("Wrong number format!");
        } else if (checkNumber(number)){
            this.number = number;
        } else if (!checkNumber(number)) {
            this.number = "";
        }
    }
    
    public LocalDateTime getTimeCreated() {
        return timeCreated;
    }
    
    public void setTimeCreated(LocalDateTime timeCreated) {
        this.timeCreated = timeCreated;
    }
    
    public LocalDateTime getTimeEdited() {
        return timeEdited;
    }
    
    public void setTimeEdited(LocalDateTime timeEdited) {
        this.timeEdited = timeEdited;
    }
    
    public boolean isPerson() {
        return isPerson;
    }
    
    public void setPerson(boolean person) {
        isPerson = person;
    }
    
    boolean hasNumber() {
        return number.length() != 0;
    }
    
    private boolean checkNumber(String number) {
        Pattern pattern = Pattern.compile("([+?\\w]{2,}(\\s|-)[(]\\w{2,}[)]|\\+?[(]\\w{2,}[)](\\s|-)\\w{2,}|\\+?\\w{2,}|\\+?[(]\\w{2,}[)])((\\s|-)\\w{2,})*");
        Matcher matcher = pattern.matcher(number);
        return matcher.matches();
    }
}

class Person extends Contact {
    private String name;
    private String surname;
    private String birthDate;
    private String gender;
    
    private Pattern pattern;
    private Matcher matcher;
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getSurname() {
        return surname;
    }
    
    public void setSurname(String surname) {
        this.surname = surname;
    }
    
    public String getBirthDate() {
        return birthDate;
    }
    
    public void setBirthDate(String birthDate) {
        pattern = Pattern.compile("(0[1-9]|1[1-2])[-]((0|[1-2])[1-9]|3[0-1])");
        matcher = pattern.matcher(birthDate);
        if (!matcher.matches()) {
            this.birthDate = "[no data]";
            System.out.println("Bad birth date!");
        } else {
            this.birthDate = birthDate;
        }
    }
    
    public String getGender() {
        return gender;
    }
    
    public void setGender(String gender) {
        pattern = Pattern.compile("[FM]");
        matcher = pattern.matcher(gender);
        if (!matcher.matches()) {
            this.gender = "[no data]";
            System.out.println("Bad gender!");
        } else {
            this.gender = gender;
        }
    }
    
    @Override
    public String toString() {
        return "Name: " + name + '\n' +
                "Surname: " + surname + '\n' +
                "Birth date: " + birthDate + '\n' +
                "Gender: " + gender + '\n' +
                "Number: " + getNumber() + '\n' +
                "Time created: " + getTimeCreated() + '\n' +
                "Time last edit: " + getTimeEdited() + '\n';
    }
}

class Organization extends Contact {
    private String name;
    private String address;
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getAddress() {
        return address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    @Override
    public String toString() {
        return "Organization name: " + name + '\n' +
                "Address: " + address + '\n' +
                "Number: " + getNumber() + '\n' +
                "Time created: " + getTimeCreated().withNano(0) + '\n' +
                "Time last edit: " + getTimeEdited().withNano(0) + '\n';
    }
}

interface ContactFactory {
    Contact makeContact();
}

class PersonFactory implements ContactFactory {
    @Override
    public Contact makeContact() {
        Person person = new Person();
        person.setPerson(true);
        return person;
    }
}

class OrganizationFactory implements ContactFactory {
    @Override
    public Contact makeContact() {
        return new Organization();
    }
}
